# Introduction {#intro}

We are working with computer science students to update/re-design our [Bactome](https://bactome.helmholtz-hzi.de/cgi-bin/h-impre.cgi) website [@hornischer2018]. This document is meant to help the students understand the data that we want to display and analyse on the website, starting with bacteria, moving on to genomes, genomics, and transcriptomics.

## Bactome

The Bactome itself started out as a website to display data on bacteria - more specifically *Pseudomonas aeruginosa* - isolated from patients at different German hospitals. We in the group [Molecular Bacteriology](https://www.helmholtz-hzi.de/en/research/research-topics/bacterial-and-viral-pathogens/molecular-bacteriology/our-research/), headed by Prof. Susanne Häußler, at the Helmholtz Centre for Infection Research have been collecting clinical isolates of this, and now other species as well, for some time. We needed somewhere to store, filter, and review the data that we were generating, especially for wet lab scientists who wanted to compare "their" isolates with others from the collection, or to easily find isolates with similar properties. Therefore, the data was stored in a MySQL database behind a web front end that was easy to use for everyone.

In 2018 we published a version of the Bactome online that is accessible also from outside our institution to share our accumulated knowledge with the scientific community. Since then our collection of clinical isolates and data about them has grown and we need a more flexible database and website to comprehensively display everything. Therefore, we teamed up with Prof. Wolfgang Nejdl at the [l3s](https://www.l3s.de/en/research/excellency) and are now re-building the Bactome with the help of computer science/engineering students from the [Leibniz University](https://www.uni-hannover.de/en/) in Hannover.

## Bactome content

The Bactome consists of three main sections - [phenotype](#phenotype), [genome](#genome), and [transcriptome](#transcriptome). So far, all data come from *P. aeruginosa* isolates, but other bacterial pathogens will be added later.

### Phenotype

The 'Phenotype' section contains _phenotypes_ (observable traits, visible characteristics) recorded in the lab, as well as general information about the clinical isolates. Isolate morphology is based on bacterial colonies grown on agar plates, antibiotic resistance was tested against commonly used antibiotics, biofilm morphology was recorded with confocal laser scanning microscopy, and the virulence phenotype refers to a test done on wax moth larvae.

### Genome

Data in the 'Genome' section is based on whole genome sequencing (WGS) using the Illumina system (chapter \@ref(next-generation-sequencing)) [@Metzker2010]. The resulting data has been analysed and included in different ways; most important for now is the mutation analysis (in the current Bactome called 'SNP extraction/comparison'). Mutations were identified by mapping the sequencing reads to a single *P. aeruginosa* reference genome ([UCBPP-PA14](https://pseudomonas.com/strain/show?id=109)), and the Bactome website offers the possibility to extract identified mutations for any combination of isolates and genes [@Winsor2016; @Khaledi2020]. It is also possible to identify mutations that are enriched in one group of isolates compared to another using Fisher's exact test.

### Transcriptome

The transcriptomic data is generated similarly to the genomic data, the basic difference being the sample preparation. The approach used to analyse the data was to sequence the wild type (UCBPP-PA14 again) together with the isolates and compare the final results. Here, reads are mapped to the reference genome, but this time the number of reads per gene is counted and used as an indicator for strength of gene expression [@Wang2009; @Croucher2010]. Comparing the isolate data against the wild type yields differential gene expression, which can be downloaded from the Bactome website for any isolate and gene combination. Additionally, it is possible to use Student's *t*-test to compare normalised read counts between two groups of isolates.
