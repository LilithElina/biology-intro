# Bacteria {#bacteria-chapter}

Bacteria are one of the two domains of _Prokaryota_ - cells without a nucleus. This means that their _DNA_, usually in the form of a circular _chromosome_ and maybe one or more _plasmids_ (see chapter \@ref(genome-chapter)), is located inside the cell without anything to separate it from other intracellular components. The cells are defined by a cell wall, which provides structural integrity and protects the _intracellular_ space. Bacteria can be separated into two groups based on a staining method (the Gram stain) using difference in cell wall composition: Gram-positive and Gram-negative. Gram-positive cell walls are thick, but let through the dye so the cells can be stained, while Gram-negative cells have thinner walls, but also have an extra outer membrane as added protection (which won't let through the dye or certain kinds of e.g. antibiotics).

Bacteria are single cell organisms (microorganisms) that can be found everywhere, either as single cells or aggregated into colonies or [biofilms](#biofilms). Bacterial cells can have different shapes like spheres, rods, and even spirals. They are usually too small to see with the naked eye and are instead observed with microscopes. Bacterial colonies, on the other hand, can be observed without the help of technical instruments. Colonies are grown on solid nutrient medium in the lab, out of a single dividing cell. Different kinds of colony morphologies are described in the Bactome and shown in Figure \@ref(fig:morph). The differences in colony morphology are caused by different genetic markers that can be of interest to researchers. A small colony variant (SCV), for example, can sometimes be correlated to antibiotic resistance [@Schniederjans2017].

```{r morph, fig.cap='Morphological categories used in the Bactome.', out.width='65%'}
knitr::include_graphics("pics/pheno_morph.PNG")
```

Since bacteria are everywhere, they often come in contact with other microorganisms or other life forms and can form symbiotic or competitive relationships. One example is the gut microbiome in humans, which consists of bacteria and other microbes that can be _commensal_ (benefiting from the human host while doing no harm) or _pathogenic_ (causing disease). Pathogenic bacteria cause disease in the host usually by either attacking and damaging host cells, by releasing toxins, or indirectly by causing an over-reaction of the host immune system. Broadly speaking, this ability to cause disease is called _virulence_, which is displayed in the Bactome as the survival rate of *Galleria mellonella* (wax moth) larvae, since insects have a relatively advanced immune system, making them useful model organisms [@Ramarao2012].


## Antibiotic resistance

Antibiotics are the main 'weapon' against bacterial infections. They either kill bacteria or inhibit bacterial growth. The first antibiotics were natural compounds produced by molds or plants, like penicillin, but these days antibiotics are synthetically made and most new drugs are designed in labs based on known structures, just with enhanced properties.

Bacteria evolve resistance against antibiotics based on antibiotic functions [@Wright2010]. A general mechanism is an increased efflux, meaning the production of pumps that can transport antibiotic molecules out of the cell through the cell wall, or a decreased uptake of materials from outside the cell. More specific are mutations to alter antibiotic targets so they can't be found any more; this mechanism is important for those antibiotics that bind to certain molecules. Bacteria can also produce enzymes that specifically degrade antibiotics.

Antibiotic resistance is a threat to global health that has made the news again and again for the last few years [@WHO2020]. In order to fight or circumvent it, we have to understand it. Therefore, antibiotic resistance profiles of all our clinical isolates are recorded and correlated to genomic and transcriptomic data [@Khaledi2020].

## Biofilms

Bacteria can not only aggregate to colonies on solid media, but also to biofilm communities. In biofilms, the bacterial cells stick to each other and to a surface, where they produce components to form a three-dimensional structure around their community, called an _extracellular_ matrix. This matrix can protect the cells embedded within from the outside world. A well-known biofilm is dental plaque, but biofilms can also form in many other settings in nature and man-made structures, like ponds, pipes, and more.

Biofilms can be formed by single species or multiple; even biofilms consisting of both bacteria and fungi are known [@Elias2012]. Independent of their composition, biofilms become a health concern when they appear in connection with infections. Examples are again dental plaque, but also biofilm growth on catheters or contact lenses, and infections of different parts of the human body [@Rybtke2015]. Due to the extracellular matrix, the bacteria can stay hidden from the immune system and most antibiotics.

To add to the problem of biofilm infections in general, there is no "one biofilm". Biofilms can have very different phenotypes and, associated with those, different properties [@Thoming2020]. In order to analyse how these different phenotypes arise and how different treatment options could work against them, we cluster our clinical isolates also by biofilm phenotype (Figure \@ref(fig:biofilm)). So far, we do this by visual inspection after laser scanning microscopy, so that we can then correlate different phenotypic groups to other properties [@Erdmann2019].

```{r biofilm, fig.cap='Biofilm phenotypes shown in the Bactome.'}
knitr::include_graphics("pics/pheno_biof.PNG")
```


## *Pseudomonas aeruginosa*

*Pseudomonas aeruginosa* is a Gram-negative bacterium that can thrive in a broad variety of environments. It is the 'P' of the ESKAPE pathogens - a group of six bacterial pathogens that are a health threat due to their virulence and antibiotic resistance (i.e. ability to 'escape' antibiotics) [@DeOliveira2020]. The six pathogens are *Enterococcus faecium*, *Staphylococcus aureus*, *Klebsiella pneumoniae*, *Acinetobacter baumannii*, *Pseudomonas aeruginosa*, and *Enterobacter* species. They cause hospital-acquired infections that are often life-threatening to already sick patients and those with weak immune systems.

*P. aeruginosa* is an opportunistic _nosocomial_ pathogen, which means it uses any opportunity it gets to infect people in a hospital setting. The bacterium can infect different tissues and organs - it is often found in burn wounds and on medical devices, and it is one of the most common pathogens in lungs of patients with cystic fibrosis, a genetic disorder affecting the lung, intestinal tract, and other organs [@Johnson2019]. *P. aeruginosa* can produce multiple toxins and virulence factors, including phenazines (toxic pigments)^[Those pigments make looking at pictures of *P. aeruginosa* infections quite 'interesting', if you dare you can do a picture search.], and it has many mechanisms to be or become resistant to antibiotics.

```{r resistance, fig.cap='Antibiotic resistance phenotypes shown in the Bactome.'}
knitr::include_graphics("pics/pheno_resist.png")
```

As already shown above in Figures \@ref(fig:morph) and \@ref(fig:biofilm), *P. aeruginosa* strains can have different morphologies, both for colonies and for biofilms. The clinical isolates that Bactome contains also come with different antibiotic resistance profiles (Figure \@ref(fig:resistance)), as most isolates are resistant against at least one of the five tested antibiotics, and they also show very diverse virulence profiles. This versatility is one reason why infections with *P. aeruginosa* are so problematic, and it's also the reason why this species is so interesting to study.
